#include "widget.h"

////////////////////////
/// \brief Atelier
////////////////////////

Atelier::Atelier(QWidget *parent) :
    QWidget(parent)
{
resize(400,500);
SetMenuBar();
SetWidgets();
}

void Atelier::AddWidget()
{
    AddDialog=new QDialog;
    name=new QLineEdit;
    AcountNum=new QLineEdit;
    AcountNum->setInputMask("999 999 999");
    Balance=new QDoubleSpinBox();
    Balance->setRange(0.0,999999.99999);
    QFormLayout *AddBox=new QFormLayout();
    QPushButton *OkButton=new QPushButton ("Ok");
    QPushButton *CancelButton=new QPushButton("Cancel");
    QVBoxLayout *WindowLayout=new QVBoxLayout;
    QHBoxLayout *ButtonLayout=new QHBoxLayout;

    AddBox->addRow("Name",name);
    AddBox->addRow("Account number",AcountNum);
    AddBox->addRow("Account Balance",Balance);

    ButtonLayout->addWidget(OkButton);
    ButtonLayout->addWidget(CancelButton);
    WindowLayout->addLayout(AddBox);
    connect(CancelButton,SIGNAL(clicked()),AddDialog,SLOT(close()));
    connect(OkButton,SIGNAL(clicked()),SLOT(AddToData()));
    connect(this,SIGNAL(DataIsNotEmpty()),this,SLOT(SetButtonEnable()));
    WindowLayout->addLayout(ButtonLayout);
    AddDialog->setLayout(WindowLayout);
    AddDialog->exec();

}

void Atelier::AddToData()
{
Account NewUser;
NewUser.setName(name->text());
NewUser.setAccountNum(AcountNum->text());
NewUser.setBalance(Balance->value());

if (!NewUser.getName().isEmpty() && !NewUser.getAccountNum().isEmpty())
{
Database.push_back(NewUser);
List->addItem(Database[Database.size()-1].getName());
emit DataIsNotEmpty();
}
AddDialog->close();
}

void Atelier::SetButtonEnable()
{

    Delete->setEnabled(true);
    Edit->setEnabled(true);
    DisplayUser->setEnabled(true);

}

void Atelier::SetButtonDisabled()
{
    Delete->setEnabled(false);
    Edit->setEnabled(false);
    DisplayUser->setEnabled(false);
}

void Atelier::DeleteWidget()
{

    List->currentItem();
    QListWidgetItem *item=List->currentItem();
    int position=SearchItem(item->text());
    if (position!=-1)
    {
        Database.erase(Database.begin()+position);
    }
    delete item;
    if (Database.isEmpty())
        SetButtonDisabled();
}

void Atelier::EditWidget()
{
EditDialog=new QDialog;
QLabel NameLabel("Name");
QLabel AccountNumLabel("AccountNumber");
QLabel BalanceLabel("Balance");

    Editname=new QLineEdit ("Name");
    EditAccountNum= new QLineEdit ("Account Number");
    EditAccountNum->setInputMask("999 999 999");
    EditMoneyBalance=new QDoubleSpinBox;
    Savename=new QPushButton ("Save");
    Savename->setEnabled(false);
    SaveAccountnum= new QPushButton ("Save");
    SaveAccountnum->setEnabled(false);
    SaveBalance = new QPushButton ("Save");
    SaveBalance->setEnabled(false);
    QPushButton Close("Close");
    QGridLayout Layout;

    QListWidgetItem *item=List->currentItem();

    int position=SearchItem(item->text());

    Editname->setText(Database[position].getName());
    EditAccountNum->setText(Database[position].getAccountNum());
    EditMoneyBalance->setValue(Database[position].getBalance());
    Layout.addWidget(&NameLabel,0,0);
    Layout.addWidget(Editname,0,1);
    Layout.addWidget(Savename,0,2);
   Layout.addWidget(&AccountNumLabel,1,0);
   Layout.addWidget(EditAccountNum,1,1);
   Layout.addWidget(SaveAccountnum,1,2);
   Layout.addWidget(&BalanceLabel,2,0);
   Layout.addWidget(EditMoneyBalance,2,1);
   Layout.addWidget(SaveBalance,2,2);
   Layout.addWidget(&Close,3,2);
    EditDialog->setLayout(&Layout);
    connect(&Close,SIGNAL(clicked()),EditDialog,SLOT(close()));
    connect(Editname,SIGNAL(textEdited(QString)),SLOT(SaveNameEnable()));
    connect(Savename,SIGNAL(clicked()),SLOT(EditDataName()));
    connect(EditAccountNum,SIGNAL(textEdited(QString)),SLOT(SaveAccountNumEnable()));
    connect(SaveAccountnum,SIGNAL(clicked()),SLOT(EditDataAccountNum()));
    connect(EditMoneyBalance,SIGNAL(valueChanged(double)),SLOT(SaveBalanceEnable()));
    connect(SaveBalance,SIGNAL(clicked()),SLOT(EditDataBalance()));

    EditDialog->exec();
}

void Atelier::SaveNameEnable()
{
    Savename->setEnabled(true);
}

void Atelier::SaveAccountNumEnable()
{
    SaveAccountnum->setEnabled(true);
}

void Atelier::SaveBalanceEnable()
{
    SaveBalance->setEnabled(true);
}

void Atelier::EditDataName()
{
    QListWidgetItem *item=List->currentItem();
    int position=SearchItem(item->text());
    item->setText(Editname->text());
    Database[position].setName(Editname->text());
}

void Atelier::EditDataAccountNum()
{
    QListWidgetItem *item=List->currentItem();
    int position=SearchItem(item->text());
    Database[position].setAccountNum(EditAccountNum->text());
}

void Atelier::EditDataBalance()
{
    QListWidgetItem *item=List->currentItem();
    int position=SearchItem(item->text());
    Database[position].setBalance(EditMoneyBalance->value());
}

void Atelier::SetSearch()
{
    SearchDialog=new QDialog;
    QPushButton *Search=new QPushButton("Search");
    SearchUsername=new QLineEdit;
    QFormLayout *SearchBar=new QFormLayout;
    QHBoxLayout *SearchLayout=new QHBoxLayout;
    SearchBar->addRow("Name",SearchUsername);
    SearchLayout->addLayout(SearchBar);
    SearchLayout->addWidget(Search);
    SearchDialog->setLayout(SearchLayout);

    connect(Search,SIGNAL(clicked()),SLOT(SelectItem()));
    SearchDialog->exec();
}

void Atelier::SelectItem()
{
    QList<QListWidgetItem *>item=List->findItems(SearchUsername->text(),Qt::MatchRecursive|Qt::MatchContains|Qt::MatchFixedString);
    if(!item.isEmpty())
List->setItemSelected(item.first(),true);
SearchDialog->close();
}

void Atelier::DisplayWidget()
{

    QDialog Display;
QLineEdit name("Name");
QLineEdit AccountNum("Account Number");
QLineEdit MoneyBalance("Balance");
QHBoxLayout DisplayLayout;
QListWidgetItem *item=List->currentItem();
int position=SearchItem(item->text());
name.setText(Database[position].getName());
AccountNum.setText(Database[position].getAccountNum());
QString BalanceValue=QString::number(Database[position].getBalance());

MoneyBalance.setText(BalanceValue);
name.setReadOnly(true);
AccountNum.setReadOnly(true);
MoneyBalance.setReadOnly(true);
DisplayLayout.addWidget(&name);
DisplayLayout.addWidget(&AccountNum);
DisplayLayout.addWidget(&MoneyBalance);
Display.setLayout(&DisplayLayout);
Display.exec();
}

void Atelier::SetMenuBar()
{
    File=new QMenu("File");
    Edition=new QMenu ("Edit");
    About =new QMenu("About");
    menuBar=new QMenuBar;
    menuBar->addMenu(File);
    menuBar->addMenu(Edition);
    menuBar->addMenu(About);

    Unisoft=new QAction("Unisoft",Edition);
    Search=new QAction("Search",About);
    AboutQt=new QAction("About Qt",About);
    Quit=new QAction("Quit",File);

    File->addAction(Quit);
    Edition->addAction(Search);
    About->addAction(Unisoft);
    About->addAction(AboutQt);

    connect(Quit,SIGNAL(triggered()),this,SLOT(close()));
    connect(AboutQt,SIGNAL(triggered()),qApp,SLOT(aboutQt()));
    connect(Unisoft,SIGNAL(triggered()),SLOT(UniSoftAbout()));
    connect(Search,SIGNAL(triggered()),this,SLOT(SetSearch()));
}

void Atelier::SetWidgets()
{
    List=new QListWidget ;
    resize(width(),height()-200);
    Add=new QPushButton("Add");
    Delete=new QPushButton ("Delete");
    Edit=new QPushButton ("Edit");
    DisplayUser =new QPushButton("Display");
    QHBoxLayout *ButtonLayout=new QHBoxLayout;
    QVBoxLayout *WindowLayout=new QVBoxLayout;
    Delete->setEnabled(false);
    Edit->setEnabled(false);
    DisplayUser->setEnabled(false);
    ButtonLayout->addWidget(Add);
    ButtonLayout->addWidget(Delete);
    ButtonLayout->addWidget(Edit);
    ButtonLayout->addWidget(DisplayUser);

    WindowLayout->addWidget(menuBar);
    WindowLayout->addWidget(List);
    WindowLayout->addLayout(ButtonLayout);

    setLayout(WindowLayout);
    setWindowTitle("Atelier3");
    connect(Add,SIGNAL(clicked()),SLOT(AddWidget()));
    connect(Delete,SIGNAL(clicked()),SLOT(DeleteWidget()));
    connect(Edit,SIGNAL(clicked()),SLOT(EditWidget()));
    connect(DisplayUser,SIGNAL(clicked()),SLOT(DisplayWidget()));
}

int Atelier::SearchItem(QString ItemText)
{
    int i(0);
 for (i=0;i<Database.size();i++)
 {
     if (Database[i].getName()==ItemText)
         return i;
 }
 return -1;
}

void Atelier::UniSoftAbout()
{
    QMessageBox msgBox;
    msgBox.setText("<em>This product was developped by the team <strong>UNISOFT</strong> under the license <strong>GPL</strong></em>");
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setStandardButtons(QMessageBox::Close);
    msgBox.exec();

}


///////////////////////
/// \brief Account
//////////////////////


Account::Account()
{
    Name="";
    AccountNum="";
    Balance=0;
}

QString Account::getName()
{
    return Name;
}

QString Account::getAccountNum()
{
    return AccountNum;
}

double Account::getBalance()
{
    return Balance;
}

void Account::setName(QString name)
{
    Name=name;
}

void Account::setAccountNum(QString Num)
{
    AccountNum=Num;
}

void Account::setBalance(double balance)
{
    Balance=balance;
}
