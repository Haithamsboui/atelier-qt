/****************************************************************************
** Meta object code from reading C++ file 'widget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.1.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../untitled1/widget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'widget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.1.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Atelier_t {
    QByteArrayData data[19];
    char stringdata[253];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_Atelier_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_Atelier_t qt_meta_stringdata_Atelier = {
    {
QT_MOC_LITERAL(0, 0, 7),
QT_MOC_LITERAL(1, 8, 14),
QT_MOC_LITERAL(2, 23, 0),
QT_MOC_LITERAL(3, 24, 12),
QT_MOC_LITERAL(4, 37, 9),
QT_MOC_LITERAL(5, 47, 12),
QT_MOC_LITERAL(6, 60, 10),
QT_MOC_LITERAL(7, 71, 13),
QT_MOC_LITERAL(8, 85, 9),
QT_MOC_LITERAL(9, 95, 15),
QT_MOC_LITERAL(10, 111, 17),
QT_MOC_LITERAL(11, 129, 14),
QT_MOC_LITERAL(12, 144, 20),
QT_MOC_LITERAL(13, 165, 17),
QT_MOC_LITERAL(14, 183, 12),
QT_MOC_LITERAL(15, 196, 18),
QT_MOC_LITERAL(16, 215, 15),
QT_MOC_LITERAL(17, 231, 9),
QT_MOC_LITERAL(18, 241, 10)
    },
    "Atelier\0DataIsNotEmpty\0\0UniSoftAbout\0"
    "AddWidget\0DeleteWidget\0EditWidget\0"
    "DisplayWidget\0AddToData\0SetButtonEnable\0"
    "SetButtonDisabled\0SaveNameEnable\0"
    "SaveAccountNumEnable\0SaveBalanceEnable\0"
    "EditDataName\0EditDataAccountNum\0"
    "EditDataBalance\0SetSearch\0SelectItem\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Atelier[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   99,    2, 0x05,

 // slots: name, argc, parameters, tag, flags
       3,    0,  100,    2, 0x0a,
       4,    0,  101,    2, 0x0a,
       5,    0,  102,    2, 0x0a,
       6,    0,  103,    2, 0x0a,
       7,    0,  104,    2, 0x0a,
       8,    0,  105,    2, 0x0a,
       9,    0,  106,    2, 0x0a,
      10,    0,  107,    2, 0x0a,
      11,    0,  108,    2, 0x0a,
      12,    0,  109,    2, 0x0a,
      13,    0,  110,    2, 0x0a,
      14,    0,  111,    2, 0x0a,
      15,    0,  112,    2, 0x0a,
      16,    0,  113,    2, 0x0a,
      17,    0,  114,    2, 0x0a,
      18,    0,  115,    2, 0x0a,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Atelier::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Atelier *_t = static_cast<Atelier *>(_o);
        switch (_id) {
        case 0: _t->DataIsNotEmpty(); break;
        case 1: _t->UniSoftAbout(); break;
        case 2: _t->AddWidget(); break;
        case 3: _t->DeleteWidget(); break;
        case 4: _t->EditWidget(); break;
        case 5: _t->DisplayWidget(); break;
        case 6: _t->AddToData(); break;
        case 7: _t->SetButtonEnable(); break;
        case 8: _t->SetButtonDisabled(); break;
        case 9: _t->SaveNameEnable(); break;
        case 10: _t->SaveAccountNumEnable(); break;
        case 11: _t->SaveBalanceEnable(); break;
        case 12: _t->EditDataName(); break;
        case 13: _t->EditDataAccountNum(); break;
        case 14: _t->EditDataBalance(); break;
        case 15: _t->SetSearch(); break;
        case 16: _t->SelectItem(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Atelier::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Atelier::DataIsNotEmpty)) {
                *result = 0;
            }
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject Atelier::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Atelier.data,
      qt_meta_data_Atelier,  qt_static_metacall, 0, 0}
};


const QMetaObject *Atelier::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Atelier::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Atelier.stringdata))
        return static_cast<void*>(const_cast< Atelier*>(this));
    return QWidget::qt_metacast(_clname);
}

int Atelier::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 17;
    }
    return _id;
}

// SIGNAL 0
void Atelier::DataIsNotEmpty()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
