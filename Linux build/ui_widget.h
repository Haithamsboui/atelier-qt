/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 5.1.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QListWidget *List;
    QPushButton *Display;
    QPushButton *Add;
    QPushButton *Delete;
    QPushButton *Edit;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QStringLiteral("Widget"));
        Widget->resize(504, 381);
        List = new QListWidget(Widget);
        List->setObjectName(QStringLiteral("List"));
        List->setGeometry(QRect(20, 70, 431, 221));
        Display = new QPushButton(Widget);
        Display->setObjectName(QStringLiteral("Display"));
        Display->setGeometry(QRect(360, 330, 80, 23));
        Add = new QPushButton(Widget);
        Add->setObjectName(QStringLiteral("Add"));
        Add->setGeometry(QRect(20, 330, 80, 23));
        Delete = new QPushButton(Widget);
        Delete->setObjectName(QStringLiteral("Delete"));
        Delete->setGeometry(QRect(130, 330, 80, 23));
        Edit = new QPushButton(Widget);
        Edit->setObjectName(QStringLiteral("Edit"));
        Edit->setGeometry(QRect(240, 330, 80, 23));

        retranslateUi(Widget);

        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QApplication::translate("Widget", "Widget", 0));
        Display->setText(QApplication::translate("Widget", "Display User", 0));
        Add->setText(QApplication::translate("Widget", "Add User", 0));
        Delete->setText(QApplication::translate("Widget", "Delete User", 0));
        Edit->setText(QApplication::translate("Widget", "Edit User", 0));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
