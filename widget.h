﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QFormLayout>
#include <QDoubleSpinBox>
#include <QListWidget>
#include <QMenuBar>
#include <QAction>
#include <QApplication>
#include <QMessageBox>
#include <QVector>
#include "adduser.h"
#include <QListWidgetItem>
#include <QList>
///////////////////////////////
/// \brief The Account class///
///////////////////////////////

class Account
{

public :

    Account();
    QString getName();
    QString getAccountNum();
    double  getBalance();
    void setName(QString name);
    void setAccountNum(QString Num);
    void  setBalance(double balance);

private :

    QString Name;
    QString AccountNum;
    double Balance;

};

///////////////////////////////
/// \brief The Atelier class///
///////////////////////////////

class Atelier : public QWidget
{
    Q_OBJECT

public:

  Atelier(QWidget *parent = 0);
  void SetMenuBar();
  void SetWidgets();
  int SearchItem(QString ItemText);

public slots:

  void UniSoftAbout();
  void AddWidget();
  void DeleteWidget();
  void EditWidget();
  void DisplayWidget();
  void AddToData();
  void SetButtonEnable();
  void SetButtonDisabled();
  void SaveNameEnable();
  void SaveAccountNumEnable();
  void SaveBalanceEnable();
  void EditDataName();
  void EditDataAccountNum();
  void EditDataBalance();
  void SetSearch();
  void SelectItem();
signals :
  void DataIsNotEmpty();

private:
  QLineEdit *SearchUsername;
  QDialog *SearchDialog;
  QPushButton *Savename;
  QPushButton *SaveAccountnum;
  QPushButton *SaveBalance;
    QLineEdit *Editname;
    QLineEdit *EditAccountNum;
    QDoubleSpinBox *EditMoneyBalance;
    QDialog *EditDialog;
    QDialog *AddDialog;
    QLineEdit *name;
    QLineEdit *AcountNum;
    QDoubleSpinBox *Balance;
    QMenuBar *menuBar;
    QMenu* File;
    QMenu* Edition;
    QMenu* About;
    QAction* Unisoft;
    QAction* Search ;
    QAction* AboutQt;
    QAction* Quit;
    QListWidget *List;
    QPushButton *Add;
    QPushButton *Delete;
    QPushButton *Edit;
    QPushButton *DisplayUser;
    QVector <Account> Database;
};


#endif // WIDGET_H


